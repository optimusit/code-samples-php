<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\backend\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = [
    'label' => 'Users',
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'db_host',
            'active',
            'acct_type',
            'reg_date',
            'mailing_addr_id',
            'billing_addr_id',
            'beta',
            'last_access',
            'promo_code',
            'reg_code',
            'nanny_status',
            'site',
            'geocode',
            'address.title',
            'address.first_name',
            'address.middle_initial',
            'address.last_name',
            'address.suffix',
            'address.company',
            'address.addr1',
            'address.addr2',
            'address.addr3',
            'address.city',
            'address.state',
            'address.zip',
            'address.country',
            'address.phone',
            'address.fax',
            'address.email',
            'address.url',
            'address.foreign_addr',
        ],
    ]) ?>

</div>
