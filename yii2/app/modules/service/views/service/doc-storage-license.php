<?php
    use yii\bootstrap\Modal;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-10">
        <p>
            Welcome to SageFire's eDocument Storage!  We are excited to get you started.
        <p>
            eDocument Storage provides a very efficient method for <?=Yii::$app->params['companyName']?> subscribers to follow-up with their prospects and customers,
            and maximize their business opportunities.  It's simple! Import the scanned image of your receipt or document from your computer and link it
            to your specific transaction.
        </p>
        <p>
            Just like Opportunity Tracker, each transaction can be linked to a specific transaction. As an example, if you
            have an extraordinary expense associated with a large sales meeting, import the receipt, a photo of your group, and the original letter
            of invitation, and link them together using eDocument Storage.
        </p>
        <p>
            During the Beta Release, please keep your original receipts as additional back-up.  Because this is a Beta release, please be aware that
            the layout or formatting of this system could change at any moment.  If you see anything you would like changed, encounter any strange
            behavior, or would like to give us a praise, please feel free to give us feedback!
        </p>
        <p>
            Once turned on, eDocument Storage is embedded within your <?=Yii::$app->params['companyName']?> account, and a centralized eDocument Storage module
            within Premium Services.
        <p>
            <b>Next</b>, please read and agree to the <?=Yii::$app->params['companyName']?> License Agreement.
            <?php
            Modal::begin([
                'header' => '<h2>'.Yii::$app->params['companyName'].' License Agreement</h2>',
                'toggleButton' => [
                    'label' => 'Read it here',
                    'class' => 'btn btn-primary'
                ],
                'options' =>['class' => 'license-text']
            ]);

            echo Yii::$app->controller->renderPartial('_license');

            Modal::end();
            ?>
        </p>
        <p>
            By clicking "I Agree", you agree to all terms and conditions.
            <?php
            $form = ActiveForm::begin([
                'action' => Url::to(['doc-storage-enable'])
            ]);
            echo Html::submitButton('I Agree', [
                'class' => 'btn btn-success'
            ]);
            $form::end();
            ?>
        </p>
    </div>
    <div class="col-md-2">
        <img align=right src="/images/dollarguy.jpg" height="300px">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <br>
        <a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">No Thanks. Take me to Premium Services.</a>
    </div>
</div>
