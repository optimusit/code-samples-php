<?php

namespace app\modules\service\controllers;

use Yii;
use app\modules\service\components\Controller;

/**
 * OpportunityMarketingController
 */
class OpportunityMarketingController extends Controller
{
    public function actionIndex()
    {
        $this->titlePage = "Opportunity marketing";

        return $this->render('index');
    }

}
