<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
    <div style="width:100%; " class="text-center">
        <h1><?=$this->context->titlePage;?></h1>
    </div>
    <div style="width:100%;  " class="text-center">
        <table width="100%" border="0">
            <tr>
                <td align="center"><h4><?= $order->billing_company ?></h4></td>
            </tr>
            <tr>
                <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
            </tr>
        </table>
    </div>
    <br>
    <table class="table-bordered-report" width="100%"
        <?php

        if ($output != ReportFilterForm::VIEW_PDF) {
            echo ' border="1"';
        }
        ?>
        >
        <?php
        if ($type_view!=ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
            <tr>
                <td><b>Date</b></td>
                <td><b>Auto</b></td>
                <td><b>Starting <br> Odometer</b></td>
                <td><b>Ending<br> Odometer</b></td>
                <td><b>Business <br>Trip Mileage</b></td>
                <td><b>Memo</b></td>
            </tr>

            <?php
            foreach ($data["detailed"] as $item){
            ?>
            <tr>
                <td ><?=Helper::toAppDate($item["date"])?></td>
                <td><?=$item["auto_name"]?></td>
                <td class="text-right  column-number"><?=Helper::formatNumber($item["odo_start"],'')?></td>
                <td class="text-right"><?=Helper::formatNumber($item["odo_end"],'')?></td>
                <td class="text-right"><?=Helper::formatNumber($item["mileage"],'')?></td>
                <td><?=$item["note"]?></td>
            </tr>
            <?php
            }
        }

        if ($type_view==ReportFilterForm::VIEW_TYPE_SHORT) {
            ?>
            <tr>
                <td><b>Auto</b></td>
                <td><b>Business Trip Mileage</b></td>
                <td><b>Memo</b></td>
            </tr>

        <?php
        }
        $total = 0;
        foreach ($data["short"] as $item){
            $total += $item["mileage"];
            ?>
            <tr>
                <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?4:0;?>" ><b><?=$item["auto_name"]?> Subtotal:</b></td>
                <td class="text-right"><b><?=Helper::formatNumber($item["mileage"],'')?></b></td>
                <td></td>
            </tr>
        <?php
        }
        ?>
        <tr>
            <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?4:0;?>" ><b>Total:</b></td>
            <td class="text-right"><b><?=Helper::formatNumber($total,'')?></b></td>
            <td></td>
        </tr>
  </table>
