<?php
use common\components\Helper;
use app\models\ReportFilterForm;

$income_div = $data["income_div"];
$unknown = "";
if (isset($data["c"]["Unknown category - to be resolved"]) || isset($data["c"]["Imported Items"])) {
    $unknown = "<sup> *</sup>";
}


?>
<div style="width:100%; " class="text-center">
    <h1><?=$this->context->titlePage;?></h1>
</div>
<div style="width:100%;  " class="text-center">
    <table width="100%" border="0">
        <tr>
            <td align="center"><h4><?= $order->billing_company ?></h4></td>
        </tr>
        <tr>
            <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
        </tr>
    </table>
</div>
<br>
<table class="table-bordered-report" width="100%"
    <?php
    if ($output != ReportFilterForm::VIEW_PDF) {
        echo ' border="1"';
    }
    ?>
    >
    <tr>
        <td colspan="3"><h3>Income</h3></td>
    </tr>
    <tr>
        <td class="column-text-margin">
            Gross receipts or sales
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["c"]["Gross receipts or sales"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?= Helper::formatPercent($currPercent); ?>
        </td>
    </tr>
    <tr>
        <td class="column-text-margin">
            Returns and allowances
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["c"]["Returns and allowances"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?= Helper::formatPercent($currPercent); ?>
        </td>
    </tr>
    <tr>
        <td class="column-text-margin">
            Net receipts and sales
        </td>
        <td class="column-number">
            <?php
            $currValue = $data["sub_tot1"];
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?= Helper::formatPercent($currPercent); ?>
        </td>
    </tr>
    <tr>
        <td class="column-text-margin">
            Purchases - goods to be sold
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["c"]["Purchases - goods to be sold"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?= Helper::formatPercent($currPercent); ?>
        </td>
    </tr>
    <tr>
        <td><b>Gross profit</b></td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["sub_tot2"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>
    <tr>
        <td class="column-text-margin">
            Sales tax collected
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["c"]["Sales tax collected"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?= Helper::formatPercent($currPercent); ?>
        </td>
    </tr>
    <tr>
        <td>
            <b>Gross income <?= $unknown ?></b>
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["income"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>

    <tr>
        <td colspan="3"><h3>Expense</h3></td>
    </tr>
    <?php
    $row = array(
        "Advertising",
        'Bad debts from sales or services',
        'Car and truck expenses',
        'Commissions and fees',
        'Employee benefit programs',
        'Insurance',
        'Interest - mortgage',
        'Interest - other',
        'Legal and professional services',
        'Office expense',
        'Other expenses',
        'Payroll taxes and withholding',
        'Pension and profit-sharing plans',
        'Rent or lease - machinery and equipment',
        'Rent or lease - other business property',
        'Rent or lease - vehicles',
        'Repairs and maintenance',
        'Sales tax remitted',
        'Supplies',
        'Taxes and licenses',
        'Travel, meals, ent. - meals & entertainment',
        'Travel, meals, ent. - travel',
        'Utilities',
        'Wages'
    );
    while (list($key) = each($row)) {
        ?>

        <tr>
            <td class="column-text-margin">
                <?= $row[$key] ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["$row[$key]"]);
                $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?= Helper::formatPercent($currPercent); ?>
            </td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td>
            <b>Total expenses <?= $unknown ?></b>
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["total_expenses"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>
    <tr>
        <td><b>Net ordinary income</b></td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["income"]) + Helper::getIntegerValue($data["total_expenses"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>
    <tr>
        <td class="column-text-margin">Other income</td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["c"]["Other income"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>
    <tr>
        <td><b>Net income <?= $unknown ?></b></td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["income"]) + Helper::getIntegerValue($data["c"]["Other income"]) + Helper::getIntegerValue($data["total_expenses"]);
            $currPercent = $income_div == 0 ? "na " : $currValue / $income_div;
            echo '<b>' . Helper::formatNumber($currValue) . '</b>';
            ?>
        </td>
        <td class="column-number">
            <?= '<b>' . Helper::formatPercent($currPercent) . '</b>'; ?>
        </td>
    </tr>
    <tr>
        <td><h3>Other business activity</h3></td>
        <td colspan="2">from <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Funds In</td>
        <td>Funds Out</td>
    </tr>
    <?php
    $need_space = false;
    if (isset($data["c"]["Unknown category - to be resolved"])) {
        $need_space = true;
        ?>
        <tr>
            <td class="column-text-margin">Unknown category - to be resolved</td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ci"]["Unknown category - to be resolved"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["co"]["Unknown category - to be resolved"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>

    <?php
    }
    if (isset($data["c"]["Imported Items"])) {
        $need_space = true;
        ?>
        <tr>
            <td class="column-text-margin">Imported Items</td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ci"]["Imported Items"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["co"]["Imported Items"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>

    <?php
    }
    if ($need_space == true) {
        ?>
        <tr>
            <td colspan="3"></td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td class="column-text-margin">Owner investment</td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["ci"]["Owner investment"]);
            echo Helper::formatNumber($currValue);
            ?>
        </td>
        <td class="column-number">
            <?php
            $currValue = Helper::getIntegerValue($data["co"]["Owner investment"]);
            echo Helper::formatNumber($currValue);
            ?>
        </td>
    </tr>
    <?php
    $row = array(
        'Asset',
        'Debt payment',
        'Transfer funds',
        'Owner charitable contributions',
        'Owner draw',
        'Owner income tax payments federal',
        'Owner income tax payments state',
        'Owner health insurance',
        'Prepaid deposits',
        'Purchases - goods for personal use'
    );
    while (list($key) = each($row)) {
        ?>
        <tr>
            <td class="column-text-margin"><?= $row[$key] ?></td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ci"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["co"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>

    <?php
    }
    ?>
    <tr>
        <td><h3>Loan activity</h3></td>
        <td colspan="2">from <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Asset loans (Loans owed to your business)</td>
        <td>Increase</td>
        <td>Decrease</td>
    </tr>
    <?php
    $row = array('Owner investment', 'Asset', 'Debt payment', 'Transfer funds');
    while (list($key) = each($row)) {
        ?>
        <tr>
            <td class="column-text-margin"><?= $row[$key] ?></td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ali"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ald"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>

    <?php
    }
    ?>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td>Asset loans (Loans owed to your business)</td>
        <td colspan="2"></td>
    </tr>
    <?php
    $row = array('Owner investment', 'Asset', 'Debt payment', 'Transfer funds');
    while (list($key) = each($row)) {
        ?>
        <tr>
            <td class="column-text-margin"><?= $row[$key] ?></td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["lli"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["lld"][$row[$key]]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>

    <?php
    }
    ?>
    <tr>
        <td><h3>Other information</h3></td>
        <td colspan="2">from <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td class="column-text-margin">Business mileage - all vehicles</td>
        <td>
            miles
        </td>
        <td class="column-number">
            <?php
            echo Helper::formatNumber($data["mileage"], '');
            ?>
        </td>
    </tr>

</table>
<br>
<table width="100%">
    <?php
    if ($unknown != "") {
        ?>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <sup>* </sup>There are transactions in your account that have not been fully catagorized. Please review
                and fully catagorize any <i>Unknown category - to be resolved</i> or <i>Imported Items</i> transactions
                under in <b>Other business activity</b>.
            </td>
        </tr>
    <?php
    }
    ?>
</table>
