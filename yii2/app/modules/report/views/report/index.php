<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\report\components\Report;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;

?>

<?php
$form = ActiveForm::begin([
    'options' => [
        'id' => 'reportForm'
    ],
]);
?>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'year',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-2 no-margin">{input}</div>'])
                ->widget(DatePicker::className(), [
                    'addon' => '',
                    'clientOptions' => [
                        'format' => " yyyy", // Notice the Extra space at the beginning
                        'viewMode' => "years",
                        'minViewMode' => "years",
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'setDate' =>' new Date()',
                    ],
                    'clientEvents' => [
                        'change' => 'function () { $( "#reportForm" ).submit(); }'
                    ]
                ])
                ->label('Year');
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 ">
        <?php
        echo $this->render('@app/views/layouts/parts/_sidebar_info_widget', [
            'infoWidget' => $infoTransaction
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <?php
        echo $this->render('@app/views/layouts/parts/_sidebar_info_widget', [
            'infoWidget' => $infoAuto
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <?php
        echo $this->render('@app/views/layouts/parts/_sidebar_info_widget', [
            'infoWidget' => $infoStat
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
<?php
   echo $form->field($model, 'report',
        ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
        ->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Report::getAll(), 'view_part', 'name'),
            'options' => [
                'placeholder' => 'Select the Report',
            ],
            'pluginEvents' => ['select2:select' =>
                'function() {
                    url="/report/report/view-form?report="+$(this).val()+"&year="+$("#reportform-year").val()+" "
                     $.pjax({url: url, container: "#content-pjax-container", push: false })
                }'
            ],

        ]);
?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
    <?php
        Pjax::begin();
    ?>
    <diw class="row" id="content-pjax-container">

    </div>
    <?php
        Pjax::end();
    ?>

</diw>