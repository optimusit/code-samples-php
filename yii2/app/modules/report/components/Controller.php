<?php
namespace app\modules\report\components;

use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/report' => 'Reports',
    ];

}
