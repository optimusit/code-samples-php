<?php
namespace app\modules\account\components;

use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/account' => 'Balance Accounts',
    ];

}
