<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\GridView;
use yii\widgets\Pjax;
use kartik\icons\Icon;

/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnAdd" data-url="<?= Url::to([
                        'create',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add New
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'communication-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'table',
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Sent',
                'value' => function ($model) {
                    return $model->date_sent;
                },
                'contentOptions' => ['style' => 'width:130px;'],
            ],
            [
                 'label' => 'Type',
                 'value' => function ($model) {
                     $val = $model->getType();
                     if ($model->to_id == Yii::$app->user->identity->getId()){
                         $val =Icon::show('arrow-down',['class'=>'btn-link']).' '.$val;
                     }
                     else {
                         $val =Icon::show('arrow-up',['class'=>'btn-link']).' '.$val;
                     }

                      return $val;
                 },
                 'contentOptions' => ['style' => 'width:150px;','class'=>'text-left'],
                 'format' =>'raw'
            ],
            [
                'label' => 'Subject',
                'value' => function ($model) {
                    $val =$model->subject;
                    if ($model->new_replies==1){
                        $val =Icon::show('inbox',['class'=>'btn-link']).' '.$val;
                    }

                    return $val;
                },
                'contentOptions'=> ['class'=>'text-left'],
                'format' =>'raw'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'View',
                'template' => '{update}',
                'contentOptions' => ['style' => 'width:50px;'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button(Icon::show('newspaper-o',['class'=>'btn-link']), [
                            'title' => 'Answers',
                            'data-action' => 'answer',
                            'class' => 'btn btn-link update',
                            'data-url' => Url::to([
                                'communicate/answer',
                                'id' => $model->id
                            ])
                        ]);
                    }
                ],
            ],
            [
                'attribute' => 'date_read',
                'contentOptions' => ['style' => 'display:none;']
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modal',
    'modalHeader' => 'Message',
    'selectorActivation' => '#btnAdd, [data-action="update"]',
    'formId' => 'message-form',
    'gridPjaxId' => 'communication-pjax-grid'
]) ?>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modalAnswer',
    'modalHeader' => 'Message',
    'selectorActivation' => '[data-action="answer"]',
    'formId' => 'answer-form',
    'gridPjaxId' => 'communication-pjax-grid'
]) ?>

