<?php
namespace app\modules\invoice\controllers;

use Yii;
use common\models\frontend\InvoiceLineItem;
use common\models\frontend\Invoice;
use common\models\frontend\InvoicePayment;
use common\models\frontend\File;
use app\modules\invoice\components\Controller;
use common\models\frontend\InvoiceSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;

class InvoiceController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Invoice();
        $data = Invoice::getLastNumber();
        $last_inv_number = '';
        if ($data) {
            $last_inv_number = $data->inv_number;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
                'last_inv_number' => $last_inv_number
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findInvoice($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionVoid($id)
    {
        $model = $this->findInvoice($id);
        $model->status = 3;
        $model->save();

        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionPrint($id)
    {
        $invoice = $this->findInvoice($id);
        $order = $this->findOrder();
        $invoiceItems = InvoiceLineItem::find()
            ->select([
                '{{INVOICE_LINE_ITEMS}}.*',
                '{{INVOICE_ITEMS}}.name as item_name',
                'round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) as total_sum_no_tax',
                'CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*{{INVOICES}}.tax_rate)/100,2) ELSE 0 END  as total_sum_tax',
                'CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*(100+{{INVOICES}}.tax_rate))/100,2) ELSE round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) END  as total_sum_with_tax',
            ])
            ->joinWith('invoice')
            ->joinWith('invoiceItem')
            ->where(['invoice_id' => $id])
            ->orderBy(['date_added' => SORT_DESC])->all();

        $logo = new File();
        $logo = $logo->find()->where(['purpose' =>FILE::LOGO_PURPOSE])->one();

        $content = $this->renderPartial('print', [
            'invoice' => $invoice,
            'order' => $order,
            'invoiceItems' => $invoiceItems,
            'logo' =>$logo
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => 'css/pdf/print.css'
            ,
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => [
                    'Invoice #' . $invoice->inv_number . ', ' . Yii::$app->formatter->asDate($invoice->date,
                        'MM/dd/yyyy')
                ],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);



        // return the pdf output as per the destination setting
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
        //return $content;
    }

    public function actionItems($id, $retURL = null)
    {
        $invoice = $this->findInvoice($id);
        $dataProvider = new ActiveDataProvider([
            'query' => InvoiceLineItem::find()
                ->select([
                    '{{INVOICE_LINE_ITEMS}}.*',
                    'round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) as total_sum_no_tax',
                    'CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*{{INVOICES}}.tax_rate)/100,2) ELSE 0 END  as total_sum_tax',
                    'CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*(100+{{INVOICES}}.tax_rate))/100,2) ELSE round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) END  as total_sum_with_tax',
                ])
                ->joinWith('invoice')
                ->where(['invoice_id' => $id])
                ->orderBy(['date_added' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('items', [
            'invoice' => $invoice,
            'dataProvider' => $dataProvider,
            'retURL' => $retURL,
        ]);
    }

    public function actionItem($invoiceId, $id = null)
    {
        $invoice = $this->findInvoice($invoiceId);
        if ($id) {
            $invoiceLineItem = $this->findInvoiceLineItem($id);
        } else {
            $invoiceLineItem = new InvoiceLineItem();
            $invoiceLineItem->invoice_id = $invoice->id;
        }
        if ($invoiceLineItem->load(Yii::$app->request->post()) && $invoiceLineItem->save()) {
        } else {
            return $this->renderAjax('_item_form', [
                'invoice' => $invoice,
                'invoiceLineItem' => $invoiceLineItem,
            ]);
        }
    }

    public function actionDeleteInvoiceLineItem($id)
    {
        $invoiceLineItem = $this->findInvoiceLineItem($id);
        $invoice = $invoiceLineItem->getInvoice()
            ->one();
        $invoiceLineItem->delete();
        $dataProvider = new ActiveDataProvider([
            'query' => InvoiceLineItem::find()
                ->where(['invoice_id' => $invoice->id])
                ->orderBy(['date_added' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('items', [
            'invoice' => $invoice,
            'dataProvider' => $dataProvider,
            'invoiceLineItem' => $invoiceLineItem,
            'retURL' => null,
        ]);
    }

    public function actionPayments($id, $retURL = null)
    {
        $invoice = $this->findInvoice($id);
        $dataProvider = new ActiveDataProvider([
            'query' => InvoicePayment::find()
                ->where(['invoice_id' => $id])
                ->orderBy(['date_entered' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('payments', [
            'invoice' => $invoice,
            'dataProvider' => $dataProvider,
            'retURL' => $retURL,
        ]);


    }

    public function actionPayment($invoiceId, $id = null)
    {
        $invoice = $this->findInvoice($invoiceId);
        if ($id) {
            $invoicePayment = $this->findInvoicePayment($id);
        } else {
            $invoicePayment = new InvoicePayment();
            $invoicePayment->invoice_id = $invoice->id;
        }
        if ($invoicePayment->load(Yii::$app->request->post()) && $invoicePayment->save()) {
        } else {
            return $this->renderAjax('_payment_form', [
                'invoice' => $invoice,
                'invoicePayment' => $invoicePayment,
            ]);
        }
    }

    public function actionDeleteInvoicePayment($id)
    {
        $invoicePayment = $this->findInvoicePayment($id);
        $invoice = $invoicePayment->getInvoice()
            ->one();
        $invoicePayment->delete();

        $dataProvider = new ActiveDataProvider([
            'query' => InvoicePayment::find()
                ->where(['invoice_id' => $invoice->id])
                ->orderBy(['date_entered' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('payments', [
            'invoice' => $invoice,
            'dataProvider' => $dataProvider,
            'invoicePayment' => $invoicePayment,
            'retURL' => null,
        ]);

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInvoice($id)
    {
        if (($model = Invoice::find()
                ->select([
                    '{{INVOICES}}.*',
                    'round(sum({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty),2) as total_sum_without_tax',
                    'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*{{INVOICES}}.tax_rate)/100,2) ELSE 0 END)  as total_sum_tax',
                    'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*(100+{{INVOICES}}.tax_rate))/100,2) ELSE round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) END)  as total_sum_with_tax',
                    '{{SOURCE}}.name  as customer_name',
                ])
                ->joinWith('invoiceLineItem')
                ->joinWith('source')
                ->where(['{{INVOICES}}.id' => $id])->one()
            ) !== null
        ) {
            $model->total_sum_payment = Invoice::getTotalSumPayment($id);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested invoice does not exist.');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InvoiceLineItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInvoiceLineItem($id)
    {
        if (($model = InvoiceLineItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested invoice line item does not exist.');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InvoicePayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInvoicePayment($id)
    {
        if (($model = InvoicePayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested payment does not exist.');
        }
    }
}
