<?php
namespace app\modules\practices\controllers;

use Yii;
use app\modules\practices\components\Controller;

/**
 * AdvisedController
 */
class AdvisedController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
