<p>
    <strong>Treat your business like a business.</strong>
</p>
<p>
    Enough said ? right?
</p>
<p>
    Here?s the finer print:  According to the IRS ?
    Everyone in business must keep records... You must keep your records as long as they may be needed...? (IRS Pub 583).
    If you have business losses, you can offset other income with those business losses.
    What does that mean to you? <strong>Less taxes to pay!</strong>
    You can deduct losses from your business activity as long as you treat that business as a real business.<br>
    If you do not, the IRS can and will reclassify your business as a Hobby and limit the losses.<br>
    What would that mean to you?<br> <br><strong>More taxes to pay!</strong><br><br>
    Follow the rest of these Best Practices and use <?php echo  Yii::$app->params['companyName']; ?> keep accurate and timely records of your business.
</p>
