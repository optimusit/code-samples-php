<?php
namespace app\modules\profile\controllers;

use Yii;
use app\modules\profile\components\Controller;
use common\components\TaxSpecialist;
use yii\helpers\Url;
use common\models\frontend\Preference;
use common\models\frontend\AccountantClientAffiliate;
use common\models\backend\User;


/**
 * TaxSpecialistController
 */
class TaxSpecialistController extends Controller
{
    public function actionIndex()
    {

        $specialist = TaxSpecialist::getSpecialistInfo();
        $auth_accountant = Preference::get('auth_accountant', Yii::$app->user->id);
        $not_auth_accountant = Preference::get('not_auth_accountant', Yii::$app->user->id);
        $message = '';

        return $this->render('index', [
            'auth_accountant' => $auth_accountant,
            'not_auth_accountant' => $not_auth_accountant,
            'specialist' => $specialist,
            'message' => $message
        ]);
    }

    public function actionGrant()
    {
        $specialistID = TaxSpecialist::getSpecialistID();
        Preference::set('auth_accountant', $specialistID, Yii::$app->user->id);
        Preference::set('not_auth_accountant', '', Yii::$app->user->id);
        $this->redirect(Url::to('/profile/tax-specialist'));
    }

    public function actionRemove()
    {
        $specialistID = TaxSpecialist::getSpecialistID();
        Preference::set('auth_accountant', '', Yii::$app->user->id);
        Preference::set('not_auth_accountant', $specialistID, Yii::$app->user->id);
        $this->redirect(Url::to('/profile/tax-specialist'));
    }

    public function actionUnload()
    {
        $specialistID = TaxSpecialist::getSpecialistID();
        AccountantClientAffiliate::$userDBName = $specialistID;
        $accountantClient = AccountantClientAffiliate::find()
            ->where(['client_id' => Yii::$app->user->id])
            ->one();
        if ($accountantClient->delete()) {
            Preference::set('auth_accountant', '', Yii::$app->user->id);
            Preference::set('not_auth_accountant', '', Yii::$app->user->id);
        }
        $this->redirect(Url::to('/profile/tax-specialist'));
    }

    public function actionLoad()
    {
        //A6411-4677  use `c0210eab3e518b36f11b2498a9e2c7aa`
        $request = Yii::$app->request;
        $specialist_code = $request->post('specialist_code');
        $specialistID = TaxSpecialist::getByCode($specialist_code);
        if (!$specialistID) {
            Yii::$app->getSession()
                ->setFlash('error', "Specialist not found!");
            $this->redirect(Url::to('/profile/tax-specialist'));

            return;
        }
        $check = TaxSpecialist::CheckDataStructure($specialistID, true);
        if ($check) {
            Preference::set('auth_accountant', $specialistID, Yii::$app->user->id);
            Preference::set('not_auth_accountant', '', Yii::$app->user->id);

            $specialistData = USER::find()
                ->where(['id' => $specialistID])
                ->one();
            if ($specialistData) {
                $accountantClient = new AccountantClientAffiliate();
                $accountantClient::$userDBName = $specialistID;
                $accountantClient->client_id = $specialistData->id;
                $accountantClient->username = $specialistData->username;
                $accountantClient->date_added = (new \DateTime())->format(Yii::$app->helper->getStorageDateTimeFormat());
                $accountantClient->address_id = $specialistData->mailing_addr_id;
                $accountantClient->active = $specialistData->active;
                $accountantClient->save();

            }
        }
        $this->redirect(Url::to('/profile/tax-specialist'));

        return;
    }

}
