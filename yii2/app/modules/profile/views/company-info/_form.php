<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use common\models\backend\ZipCode;

/* @var $this yii\web\View */
/* @var $order \common\models\backend\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>
<?php if (Yii::$app->request->isPjax && !$order->hasErrors()) { ?>
    <div class="alert alert-info" role="alert">Successful</div>
<?php } ?>
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'form-horizontal',
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-8\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
    ],
]); ?>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_company')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_address1')
            ->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_firstname')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_address2')
            ->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_initial')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_city')
            ->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_lastname')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_state')
            ->widget(Select2::classname(), [
                'data' => ArrayHelper::map(ZipCode::find()
                    ->select('state')
                    ->distinct()
                    ->orderBy('state')
                    ->asArray()
                    ->all(), 'state', 'state'),
            ]) ?>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_email')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_zip')
            ->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($order, 'billing_phone')
            ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($order, 'billing_mobile')
            ->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="form-group modal-footer">
    <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span> Update', [
        'class' => 'btn btn-primary'
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
