<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\GridView;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnAdd" data-url="<?= Url::to([
                        'create',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add New
                    </button>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'tax-codes-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'table',
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'label' => 'Percent',
                'headerOptions' => ['width' => '30px;'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->percent, 4);
                },
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'multiple' => false,
                'header' => 'Default',
                'headerOptions' => ['width' => '30px;'],
                'checkboxOptions' => function ($invoice, $key, $index, $column) {
                    $options = ['name' => 'def', 'disabled' => 'disabled'];
                    if ($invoice->isDefault()) {
                        $options['checked'] = 'checked';
                    }

                    return $options;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' => ['style' => 'display:none;'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'data-action' => 'update',
                            'class' => 'btn btn-link update',
                            'data-url' => Url::to([
                                'sales-tax-code/update',
                                'id' => $model->id
                            ])
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<br>
<p>
    NOTE: Tax codes in use by Invoices or To/From's cannot be deleted or renamed.
</p>
<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modal',
    'modalHeader' => 'Tax Code',
    'selectorActivation' => '#btnAdd, [data-action="update"]',
    'formId' => 'tax-codes-form',
    'gridPjaxId' => 'tax-codes-pjax-grid'
]) ?>

