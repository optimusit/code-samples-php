<?php
namespace app\modules\profile\components;

use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/profile' => 'Company Information',
        '/profile/sales-tax-code' => 'Manage Sales Tax Codes',
        '/profile/tax-specialist' => 'Authorize Tax Specialist',
        //'/profile/billing-information' => 'Change Billing Information',
        '/profile/login-information' => 'Change Login Information',
        '/profile/invoice-logo' => 'Customize Invoice Logo',
        '/profile/subscription' => 'Subscription',
        '/profile/payment-card' => 'Payment Card',
    ];
}
