<?php
namespace app\modules\transaction\controllers;

use Yii;
use app\modules\transaction\components\Controller;

/**
 * FirstTimeController
 */
class FirstTimeInHelpController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
