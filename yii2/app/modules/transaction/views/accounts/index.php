<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\GridView;
use yii\widgets\Pjax;
use \common\models\frontend\BankAccount;
/** @var $totals BankAccount */
?>

<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnAdd" data-url="<?= Url::to([
                        'create',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add New
                    </button>

                    <button class="btn btn-success plus" id="btnAddBeginBalance" data-url="<?= Url::to([
                        'begin-balance',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add Begin Balance
                    </button>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
            </div>

        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'accounts-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'table',
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Active',
                'attribute' => 'active',
                'contentOptions' => ['style' => 'width:70px;']
            ],

            [
                'label' => 'Balance',
                'value' => 'name',
                'contentOptions' => ['class'=>'text-left'],
            ],

            'acct_type',
            'begin_balance',
            [
                'label' => 'Balance',
                'value' => function ($model) {
                    return $model->total_balance;
                },
                'contentOptions' => ['style' => 'width:100px;'],
                'footer' => $totals->sum_total_balance
            ],
            [
                'label' => 'Funds In',
                'value' => function ($model) {
                    return $model->total_in;
                },
                'footer' => $totals->sum_total_in
            ],
            [
                'label' => 'Funds Out',
                'value' => function ($model) {
                    return $model->total_out;
                },
                'footer' => $totals->sum_total_out
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' => ['style' => 'display:none;'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'data-action' => 'update',
                            'class' => 'btn btn-link update',
                            'data-url' => Url::to([
                                'accounts/update',
                                'id' => $model->id
                            ])
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modal',
    'modalHeader' => 'Account',
    'selectorActivation' => '#btnAdd, [data-action="update"]',
    'formId' => 'accounts-form',
    'gridPjaxId' => 'accounts-pjax-grid'
]) ?>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modalBalance',
    'modalHeader' => 'Setup Beginning Balance',
    'selectorActivation' => '#btnAddBeginBalance',
    'formId' => 'begin-balance-form',
    'gridPjaxId' => 'accounts-pjax-grid'
]) ?>
