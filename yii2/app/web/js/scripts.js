$(document).ready(function () {
	$(document).on('click', '.plus-text', function () {
		$(this).parent('.more-model').addClass('active');
		$(this).nextAll('.more').addClass('active');
		$('.model-content-wrapper').css('border-bottom','1px solid #b8b8b8');
		$('.model-content-wrapper').addClass('height-modal');

	});

	$(document).on('click', '.minus-text', function () {
		$(this).parent('.more-model').removeClass('active');
		$(this).nextAll('.more').removeClass('active');
		$('.model-content-wrapper').removeClass('height-modal');
		$('.model-content-wrapper').css('border-bottom','0px solid #b8b8b8');
	});

	$('.sidebar-info-widget').on('click', '.radio-button', function () {
		var parent = $('.sidebar-info-widget').first();
		parent.find('.items').addClass('hidden');
		parent.find('.' + $(this).attr('id')).removeClass('hidden');
	});

	$(document).on('click', '.actions button', function () {
		var action = $(this).data('action');
		$('.table.row-selectable tbody tr.active [data-action="' + action + '"]').trigger('click');
	});

	$(document).on('click', '.table.row-selectable tbody tr', function () {
		var row = $(this);
		gridViewSelectRow(row.parents('table:first'), row);
	});
});

function gridViewSelectRow(gridView, row) {
	gridView.find('tbody tr').removeClass('active');
	if (row) {
		gridView.find('tr[data-key="' + row.data('key') + '"]').addClass('active');
		gridViewActionsSetEnable(true);
	} else {
		gridViewActionsSetEnable(false);
	}
}

function gridViewActionsSetEnable(enabled) {
	var el = $('.actions button');
	if (enabled) {
		el.removeAttr('disabled').addClass('active');
	} else {
		el.attr('disabled', 'disabled').removeClass('active');
	}
}
