<?php

namespace app\components;

class PjaxModalForm
{
    public static function get($params)
    {
        return \Yii::$app->controller->renderPartial('@app/views/layouts/_pjaxModalForm', $params);
    }
}
