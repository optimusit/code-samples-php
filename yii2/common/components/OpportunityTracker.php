<?php
/**
 * Created by IntelliJ IDEA.
 * User: vovan
 * Date: 11.01.17
 * Time: 14:35
 */
namespace common\components;

use common\models\frontend\OpportunityDoc;
use Yii;
use common\models\frontend\Preference;
use common\models\backend\OpportunityDocOrig;

class OpportunityTracker extends \common\components\ActiveRecord
{
    const ACTIVE_ACTIVE = 'Y';
    const ACTIVE_INACTIVE = 'N';

    public static function isEnabled()
    {
        $result = Preference::find()
            ->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'opp-active'
            ])->one();
        if ($result) {
            $response = (strtoupper($result->pref_value) == self::ACTIVE_ACTIVE) ? true : false;
            return $response;
        }

        return false;
    }

    public static function Enable($flow)
    {
        $result = Preference::find()
            ->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'opp-active'
            ])->one();
        if (count($result) > 0) {
            $result->delete();
        }

        $newKey = new Preference();
        $newKey->set('opp-active', self::ACTIVE_ACTIVE, Yii::$app->user->id);
        if (!$newKey->save()) {
            return false;
        }

        OpportunityDoc::deleteAll();
        $docOrig = OpportunityDocOrig::find()
            ->where([
                'flow' => $flow
            ])->one();

        $doc = new OpportunityDoc();
        $doc->name = addslashes($docOrig->name);
        $doc->subject = addslashes($docOrig->subject);
        $doc->file = addslashes($docOrig->file);
        $doc->email_script = addslashes($docOrig->email_script);
        $doc->letter_script = addslashes($docOrig->letter_script);
        if (!$doc->save()) {
            return false;
        }

        return true;
    }

    public static function Disable()
    {
        $result = Preference::find()
            ->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'opp-active'
            ])->one();

        $result->delete();
        return true;
    }

    public static function setFlow($flow)
    {
        $preference = new Preference();
        $preference->pref_userid = Yii::$app->user->id;
        $preference->pref_key = 'OT_Flow';
        $preference->pref_value = $flow;
        if ($preference->save()) {
            return true;
        } else {
            return false;
        }
    }

}