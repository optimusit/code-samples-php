<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "LICENSE_SBR".
 *
 * @property string $id
 * @property string $user_id
 * @property string $aff_id
 * @property string $date_purch
 * @property string $used
 * @property string $type
 * @property integer $num_of_subs
 */
class LICENSESBR extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LICENSE_SBR';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['date_purch'], 'safe'],
            [['num_of_subs'], 'integer'],
            [['id', 'user_id', 'aff_id'], 'string', 'max' => 32],
            [['used'], 'string', 'max' => 1],
            [['type'], 'string', 'max' => 5],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'aff_id' => 'Aff ID',
            'date_purch' => 'Date Purch',
            'used' => 'Used',
            'type' => 'Type',
            'num_of_subs' => 'Num Of Subs',
        ];
    }
}
