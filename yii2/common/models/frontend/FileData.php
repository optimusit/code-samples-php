<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%FILEDATA}}".
 *
 * @property integer $id
 * @property integer $masterid
 * @property resource $filedata
 */
class FileData extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%FILEDATA}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['masterid'],
                'integer'
            ],
            [
                ['filedata'],
                'required'
            ],
            [
                ['filedata'],
                'string'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'masterid' => 'Masterid',
            'filedata' => 'Filedata',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'masterid']);
    }
}
