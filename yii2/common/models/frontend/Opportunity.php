<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP}}".
 *
 * @property string $id
 * @property string $date
 * @property string $address
 * @property string $status
 * @property string $classification
 */
class Opportunity extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['id'],
                'required'
            ],
            [
                ['date'],
                'safe'
            ],
            [
                [
                    'id',
                    'address'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'status',
                    'classification'
                ],
                'string',
                'max' => 50
            ],
            [
                ['address'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'address' => 'Address',
            'status' => 'Status',
            'classification' => 'Classification',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpportunityAction()
    {
        return $this->hasMany(OpportunityAction::className(), ['opp' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpportunityActionItem()
    {
        return $this->hasMany(OpportunityActionItem::className(), ['opp_action' => 'id'])
            ->viaTable(OpportunityAction::tableName(), ['opp' => 'id']);
    }
}
