<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */

$this->title = 'Confirm';
$this->params['breadcrumbs'][] = 'Confirm';
?>
Account Details
<div>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view user-confirm-registration'],
        'attributes' => [
            'code',
            'order_status',
            [
                'label' => 'Package',
                'value' => $model->package->name,
            ],
            [
                'label' => 'Subscription',
                'value' => $model->subscription->name,
            ],
            [
                'label' => 'Services',
                'value' => $this->render('_package_services2', ['model' => $model]),
                'format' => 'raw'
            ],
        ],
    ]) ?>
</div>
Contact Information
<div>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view user-confirm-registration'],
        'attributes' => [
            [
                'attribute' => 'billing_cardtype',
                'value' => $model->billingCardType()
            ],
            [
                'attribute' => 'billing_cardnumber',
                'value' => $model->getMaskedCardNumber()
            ],
            [
                'label' => 'Card Expiration',
                'value' => $model->billing_cardexp_month . ' / ' . $model->billing_cardexp_year
            ],
            [
                'label' => 'Today\'s Charge',
                'value' => sprintf('Amount: %s (%s months)',
                    Yii::$app->formatter->asCurrency($model->getMySubscriptionTotalDiscountPrice()),
                    $model->getSubscriptionObject()->recurrence)
            ],
            [
                'label' => 'Recurring Charge',
                'value' => sprintf('Amount: %s (every %s months)<br>Next Billing: %s',
                    Yii::$app->formatter->asCurrency($model->getMySubscriptionTotalDiscountPrice()),
                    (($model->getSubscriptionObject()->recurrence) > 1
                        ? $model->getSubscriptionObject()->recurrence
                        : ''), Yii::$app->formatter->asDate((new \DateTime())->add(new \DateInterval('P'
                        . $model->getSubscriptionObject()->recurrence . 'M')))),
                'format' => 'raw'
            ],
            'billing_company',
            [
                'label' => 'Name',
                'value' => $model->billing_firstname . ' ' . $model->billing_initial . ' ' . $model->billing_lastname
            ],
            [
                'label' => 'Address',
                'value' => $model->billing_address1 . '<br>' . $model->billing_city . ', ' . $model->billing_state . ' '
                    . $model->billing_zip,
                'format' => 'raw'
            ],
            [
                'label' => 'Contact',
                'value' => 'T: ' . $model->billing_phone . '<br>C: ' . $model->billing_mobile . '<br>E: '
                    . $model->billing_email,
                'format' => 'raw'
            ],
        ],
    ]) ?>
</div>
Extra information
<div>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view user-confirm-registration'],
        'attributes' => [
            [
                'attribute' => 'comments',
                'value' => nl2br(Html::encode($model->comments)),
                'format' => 'raw'
            ],
        ],
    ]) ?>
</div>
<div class="row">
    <div class="col-lg-2 col-lg-offset-4">
        <?php echo Html::a('Update', [
            'update',
            'id' => $model->id
        ], ['class' => 'btn btn-default']) ?>
    </div>
    <div class="col-lg-2 col-md-2">
        <?php echo Html::a('Confirm', [
            'confirm',
            'id' => $model->id
        ], [
            'class' => 'btn btn-primary',
            'onclick' => '$("#modalProcessing").modal("show");'
        ]) ?>
    </div>
</div>
<?= \common\widgets\ModalProcessing\ModalProcessing::widget(); ?>
