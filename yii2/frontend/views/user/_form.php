<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\backend\ZipCode;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
    $('#order-subscription_id :radio').on('change', function(){
        var selected = $(this).filter(':checked').val();
        $('.service-price').hide();
        $(document).find('table.service-price[data-subscription_id="' + selected + '"]') .show();
    });
    $('#order-billing_phone')
    .focusin(function () {
        var trimmed = $(this).data('trimmed');
        if (trimmed) {
            $(this).val(trimmed);
        }
    })
    .focusout(function () {
        var value = $(this).val();
        var fmtStr='';
        var index = 0;
        var limitCheck = value.length;
        var countnumber=0;
        while (index != limitCheck) {
            if (!isNaN(parseInt(value.charAt(index)))) {
                fmtStr = fmtStr + value.charAt(index);
                countnumber=countnumber+1;
            }
            index = index + 1;
        }
        if (fmtStr.length == 10) {
            fmtStr = '(' + fmtStr.substring(0,3) + ') ' + fmtStr.substring(3,6) + '-' + fmtStr.substring(6,10);
            $(this).val(fmtStr).data('trimmed', value);
        }
    });
JS;
$this->registerJs($js);
?>
<?= \common\widgets\ModalProcessing\ModalProcessing::widget(); ?>
<div class="registration">
    <div class="row text-center">
        <h1>Create Your KeepMore.net Account</h1>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'regform',
        'options' => [
            'class' => 'form-inline',
        ]
    ]); ?>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_email', ['enableAjaxValidation' => true,])
                ->textInput([
                    'maxlength' => true,
                    'autofocus' => true
                ]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'user_password')
                ->textInput(['maxlength' => true])
                ->passwordInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'user_password_repeat')
                ->textInput(['maxlength' => true])
                ->passwordInput() ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_company')
                ->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_firstname')
                ->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_initial')
                ->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_lastname')
                ->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_address1')
                ->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_address2')
                ->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_zip')
                ->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_city')
                ->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_state')
                ->dropDownList(ArrayHelper::map(ZipCode::find()
                    ->select('state')
                    ->distinct()
                    ->orderBy('state')
                    ->asArray()
                    ->all(), 'state', 'state')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <?= $form->field($model, 'billing_phone')
                ->textInput([
                    'maxlength' => true,
                    'data' => ['trimmed' => '']
                ]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
            <?= $form->field($model, 'billing_mobile')
                ->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row text-center">
        <h2>Comment Additional Information</h2>
    </div>
    <div class="row text-center">
        <div class="col-sm-offset-3 col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-6 xs-12 ">
            <?= $form->field($model, 'comments')
                ->textarea([
                    'maxlength' => true,
                    'class' => 'form-control input-medium',
                    'rows' => 5
                ])
                ->label(false) ?>
        </div>
    </div>

    <div class="row text-center">
        <h2>Choose your payment recurrence.</h2>
    </div>
    <div class="row text-center">
        <?= $form->field($model, 'subscription_id')
            ->radioList(ArrayHelper::map($model->getPackage()
                ->one()
                ->getSubscriptions()
                ->all(), 'id', 'name'))
            ->label(false) ?>
    </div>
    <div class="row text-center">
        <h2>Your Services</h2>
    </div>
    <div class="row text-center">
        <div class="col-sm-offset-3 col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-6 xs-12 ">
            <div class="table-responsive">
                <?= $this->render('_package_services', ['model' => $model]) ?>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <h2>Enter Your Billing Information</h2>
    </div>
    <?= $this->render('@common/views/card.php', [
        'form' => $form,
        'model' => $model
    ]); ?>
    <div class="row text-center">
        <span class="save-btn">
        <?= Html::submitButton($model->isNewRecord
            ? 'Create Account'
            : 'Update', [
            'class' => 'button'
        ]) ?>
        </span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
