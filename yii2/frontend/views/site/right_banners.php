<?php
    use yii\helpers\Html;
?>
<div class="animation-banner"><?php echo Html::a( '<img src="/images/signup-banner.jpg" alt="promo" class="img-responsive"/>', ['site/reg'],  ["title"=>"Registration"]); ?></div>
<div class="animation-banner"><?php echo Html::a( '<img src="/images/video-banner.jpg" alt="promo" class="img-responsive"/>', ['site/anim'],  ["title"=>"Animation"]); ?></div>
<div class="animation-banner"><a target="_blank" href=http://www.rackron.com/><img src="/images/rackron-logo.jpg" alt="promo" class="img-responsive"></a></div>
<div class="clearLeft" style="line-height:1px;">&nbsp;</div></div>
