<?php

/**
 * This is the model class for table "obm_developer_sale_rate".
 *
 * The followings are the available columns in table 'obm_developer_sale_rate':
 * @property string $id
 * @property string $user_id
 * @property string $rate_low_transaction_id
 * @property string $rate_avg_transaction_id
 * @property string $rate_high_transaction_id
 * @property string $planned_day_time
 * @property string $comment
 * @property string $date
 *
 * The followings are the available model relations:
 * @property MoneyTransaction $rateLowTransaction
 * @property MoneyTransaction $rateAvgTransaction
 * @property MoneyTransaction $rateHighTransaction
 * @property User $user
 */
class DeveloperSaleRate extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'obm_developer_sale_rate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'user_id, rate_low_transaction_id, rate_avg_transaction_id, rate_high_transaction_id',
                'length',
                'max' => 10
            ),
            array('planned_day_time', 'length', 'max' => 4),
            array('comment, date', 'safe'),
            array(
                'id, user_id, rate_low_transaction_id, rate_avg_transaction_id, rate_high_transaction_id, planned_day_time, comment, date',
                'safe',
                'on' => 'search'
            ),
        );
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if (($date = AppHelper::dateFromFormatApplication($this->date))) {
                $date->modify('first day of this month');
                $this->date = AppHelper::formatDateInternational($date);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rateLowTransaction' => array(
                self::BELONGS_TO,
                'MoneyTransaction',
                'rate_low_transaction_id',
                'alias' => 'rateLowTransaction',
                'together' => true,
            ),
            'rateAvgTransaction' => array(
                self::BELONGS_TO,
                'MoneyTransaction',
                'rate_avg_transaction_id',
                'alias' => 'rateAvgTransaction',
                'together' => true,
            ),
            'rateHighTransaction' => array(
                self::BELONGS_TO,
                'MoneyTransaction',
                'rate_high_transaction_id',
                'alias' => 'rateHighTransaction',
                'together' => true,
            ),
            'user' => array(self::BELONGS_TO, 'User', 'user_id', 'alias' => 'user'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'Разработчик',
            'rate_low_transaction_id' => 'Нижний рейт',
            'rate_avg_transaction_id' => 'Средний рейт',
            'rate_high_transaction_id' => 'Верхний рейт',
            'planned_day_time' => 'Время эффективной работы',
            'comment' => 'Комментарий',
            'date' => 'Дата',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('rate_low_transaction_id', $this->rate_low_transaction_id, true);
        $criteria->compare('rate_avg_transaction_id', $this->rate_avg_transaction_id, true);
        $criteria->compare('rate_high_transaction_id', $this->rate_high_transaction_id, true);
        $criteria->compare('planned_day_time', $this->planned_day_time, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DeveloperSaleRate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getDate()
    {
        return $this->isNewRecord
            ? $this->date
            : $this->formatDate($this->date);
    }
}
